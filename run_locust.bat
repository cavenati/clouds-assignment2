@REM Local
@REM locust --headless -u 32 -r 32 --csv locust_results/local/local -H http://127.0.0.1:5000 --run-time 3m

@REM Azure VM scale set
@REM locust --headless -u 32 -r 32 --csv locust_results/vm_scaleset/vm_scaleset -H http://20.8.122.28:80 --run-time 3m

@REM Azure WebApp
@REM locust --headless -u 32 -r 32 --csv locust_results/web_app/web_app -H https://clouds-a2-3.azurewebsites.net/ --run-time 3m

@REM Azure function
@REM locust --headless -u 32 -r 32 --csv locust_results/function/function -H https://integral-function.azurewebsites.net/api/ --run-time 3m
