# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt


from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient


def main(text: str) -> list:
    #TODO: move in an env vriable
    not_a_secure_way_to_store_keys = "DefaultEndpointsProtocol=https;AccountName=besteurecomcourseever;AccountKey=b+sCIylTHHaRdB9l3lidxlVV9B7sxECSs+MwXglZUNNp7yuhvve35Y29Mo6XIbQ4nKVZa94VEh52+ASt3nl/ug==;EndpointSuffix=core.windows.net"
    blob_service_client = BlobServiceClient.from_connection_string(not_a_secure_way_to_store_keys)
    container_name = "mrinput"

    # Download the blob to a local file
    # Add 'DOWNLOAD' before the .txt extension so you can see both files in the data directory
    container_client = blob_service_client.get_container_client(container= container_name) 

    text = []
    for i in range(1,5):
        data = container_client.download_blob(f'mrinput-{i}.txt').readall()
        text += data.decode("utf-8").replace('\r', '').split("\n")

    return text