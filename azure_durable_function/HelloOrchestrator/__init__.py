# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    # input = ['Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque dui magna, aliquam ut nisi sed, tempor venenatis lorem. Pellentesque tempus pharetra nibh et condimentum. Cras scelerisque enim neque, volutpat tincidunt erat fermentum nec. Vivamus quam odio, volutpat at velit eu, euismod pellentesque metus. Maecenas vel turpis laoreet, dapibus ante at, mollis elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse cursus leo sit amet mi iaculis, sit amet faucibus lorem dapibus.', 
    #     'Phasellus odio mi, suscipit a leo non, fringilla cursus velit. Donec consequat eget augue eu congue. Suspendisse eget erat lectus. In consequat massa vitae libero ultrices, in finibus ante accumsan. Aliquam ultrices, nisi vel commodo pretium, dolor ante scelerisque leo, vitae porttitor tortor quam id ante. Sed pharetra interdum est eget ultricies.',
    #     'Donec sit amet arcu non sapien pulvinar eleifend eget non sapien. Etiam elit velit,',
    #     ' elementum sit amet orci at, sollicitudin ornare tellus. Nullam a erat erat.']
    
    input = yield context.call_activity('GetInputDataFn')

    tasks = []
    for line in input:
        tasks.append(context.call_activity("Mapper", line))
    map_result = yield context.task_all(tasks)
    
    flat_map_result = [item for sublist in map_result for item in sublist]

    shuffler_result = yield context.call_activity('Shuffler', flat_map_result)
    
    tasks = []
    for word, count in shuffler_result.items():
        tasks.append(context.call_activity("Reducer", {word: count}))
    reducer_result = yield context.task_all(tasks)
    
    return [reducer_result]

main = df.Orchestrator.create(orchestrator_function)