# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging


def main(words: list) -> dict:
    result = {}
    for w in words:
        if w[0] in result:
            result[w[0]].append(w[1])
        else:
            result[w[0]] = [w[1]]
    return result
