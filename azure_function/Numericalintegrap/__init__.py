import logging

import azure.functions as func

from . import integration as ig

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')

    lower = req.route_params.get('lower')
    upper = req.route_params.get('upper')
    
    result=""
    for n in ig.N:
        i = ig.integral(ig.fn, n, float(lower), float(upper))
        result += "<p> n=" + str(n) + ": " + str(i) + "</p>"
    return func.HttpResponse(
        result,
        status_code=200,
        mimetype='text/html'
        )
    
