import numpy as np

N = [10, 100, 100, 1000, 10_000, 100_000, 1_000_000]


def integral(fn, samples_number, lower, upper):
    dx = np.divide((upper - lower), samples_number)
    Xs = np.arange(lower, upper, dx)
    integral = np.sum(fn(Xs)*dx)
    return integral


def fn(x): return np.abs(np.sin(x))


if __name__ == '__main__':

    A = 0.0
    B = np.pi

    for n in N:
        print(integral(fn, n, A, B))
