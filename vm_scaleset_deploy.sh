#!/bin/bash

sudo apt update

sudo apt install -y python3-pip

git clone https://gitlab.eurecom.fr/cavenati/clouds-assignment2

cd clouds-assignment2/

pip3 install -r requirements.txt

export FLASK_APP=app.py
